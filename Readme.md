# Mikro
Is a framework with supporting generator which generates HTTP- and GRPC-services from .proto files.

## Mikro framework

For now is almost empty, but should be enriched with needed functionality.

## Mikrogenerator for generating projects

### Installation

```
go get -u gitlab.com/bullgare/mikro/cmd/mikrogenerator
```

### Running

#### Initializing a project from .proto

```
mikrogenerator init --module=gitlab.com/bullgare/mikro/example/simple --proto=/Users/bullgare/projects/go/src/gitlab.com/bullgare/mikro/example/simple/simple.proto
```

After that just update handlers code in files
* `/Users/bullgare/projects/go/src/gitlab.com/bullgare/mikro/example/simple/internal/app/echo-service/echo.go` and
* `/Users/bullgare/projects/go/src/gitlab.com/bullgare/mikro/example/simple/internal/app/echo-service/get_info.go`

You can also update app start options at `/Users/bullgare/projects/go/src/gitlab.com/bullgare/mikro/example/simple/internal/app/echo-service/echo_service.go` 
 
 Then run the app with command
 ```
 cd /Users/bullgare/projects/go/src/gitlab.com/bullgare/mikro/example/simple/
 go run cmd/simple/main.go
 ```
 
 And request it:
 * by HTTP: `curl -X GET "http://localhost:3000/echo/message" -H "accept: application/json"`
 * by GRPC: `grpc_cli call localhost:3001 EchoService/Echo "s:'my message'"`
 
 Swagger UI is available here - `http://localhost:3002/`
 