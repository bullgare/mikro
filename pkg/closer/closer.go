package closer

import (
	"context"
	"log"
	"sync"
	"time"
)

const gracefulShutdownDelay = 1 * time.Second

type serverCloseFn func()

type ServerCloseFns struct {
	fns         []serverCloseFn
	guardFns    sync.RWMutex
	guardRunner sync.Mutex // it's here for just not to start RunAll method multiple times
}

func (f *ServerCloseFns) Ctx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), gracefulShutdownDelay)
}

func (f *ServerCloseFns) Add(fn serverCloseFn) {
	f.guardFns.Lock()
	f.fns = append(f.fns, fn)
	f.guardFns.Unlock()
}

// TODO remove logging via log package
func (f *ServerCloseFns) RunAll(err error) {
	f.guardRunner.Lock()
	f.guardFns.RLock()
	log.Printf("%v, shutting down servers", err)
	for _, fn := range f.fns {
		fn()
	}
	f.guardFns.RUnlock()
	defer f.guardRunner.Unlock() // yes, I know that it will never be invoked. it's here just for sanity

	// it is an ad-hoc for waiting for all server shutdowns to avoid many channel manipulation
	time.Sleep(gracefulShutdownDelay)
	log.Fatal("Stopped")
}
