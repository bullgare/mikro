package closer

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func CatchSignals() <-chan os.Signal {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	returningCh := make(chan os.Signal, 1)

	go func() {
		s := <-sigc
		log.Printf("OS SIGNAL CATCHED: %v\nStopping the app.\n", s)

		log.Println("Forwarding signal to the app")

		select {
		case returningCh <- s:
			log.Println("Waiting for app termination")
		case <-time.After(2 * gracefulShutdownDelay):
			log.Fatal("No respond from the app. Killing.")
		}
	}()

	return returningCh
}
