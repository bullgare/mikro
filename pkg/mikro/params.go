package mikro

// parameters that are embedded via LDFLAGS
var (
	Name      string
	Version   string
	GoVersion string
	BuildDate string
	GitLog    string
	GitHash   string
	GitBranch string
)
