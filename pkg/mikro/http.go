package mikro

import (
	"fmt"
	"net"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
)

func (a *App) startPublicHTTPServer() (*http.Server, error) {
	r := chi.NewMux()

	corsMw := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	})

	r.Use(corsMw.Handler)

	r.HandleFunc("/", http.NotFound)

	a.desc.RegisterHTTP(r)

	server := &http.Server{Handler: r}

	closeFn := func() {
		ctx, cancel := a.closeFns.Ctx()
		defer cancel()

		_ = server.Shutdown(ctx)
	}
	a.closeFns.Add(closeFn)

	listener, err := net.Listen("tcp", a.httpHost)
	if err != nil {
		return nil, fmt.Errorf("failed to create http listener: %w", err)
	}

	go func() {
		err = server.Serve(listener)
		if err != nil {
			a.closeFns.RunAll(fmt.Errorf("failed to start http server: %w", err))
		}
	}()
	return server, nil
}
