package mikro

import (
	"fmt"
	"net"
	"net/http"

	"github.com/utrack/clay/v2/transport/swagger"

	swaggerui "gitlab.com/bullgare/mikro/swagger"
)

func (a *App) startAdminHTTPServer() (*http.Server, error) {
	r := http.NewServeMux()

	r.HandleFunc("/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		r.Host = a.httpHost
		o := []swagger.Option{
			swagger.WithHost(a.httpHost),
		}

		w.Header().Set("Content-Type", "application/json")
		_, _ = w.Write(a.desc.SwaggerDef(o...))
	})

	r.Handle("/", swaggerui.NewHTTPHandler())

	server := &http.Server{Handler: r}

	closeFn := func() {
		ctx, cancel := a.closeFns.Ctx()
		defer cancel()

		_ = server.Shutdown(ctx)
	}
	a.closeFns.Add(closeFn)

	listener, err := net.Listen("tcp", a.adminHost)
	if err != nil {
		return nil, fmt.Errorf("failed to create admin listener: %w", err)
	}

	go func() {
		err = server.Serve(listener)
		if err != nil {
			a.closeFns.RunAll(fmt.Errorf("failed to start admin server: %w", err))
		}
	}()
	return server, nil
}
