package mikro

import (
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func (a *App) startGRPCServer() (*grpc.Server, error) {
	server := grpc.NewServer()

	closeFn := func() {
		ctx, cancel := a.closeFns.Ctx()
		defer cancel()

		done := make(chan struct{})

		go func() {
			server.GracefulStop()
			close(done)
		}()

		go func() {
			select {
			case <-done:
				return
			case <-ctx.Done():
				server.Stop()
				return
			}
		}()
	}
	a.closeFns.Add(closeFn)

	a.desc.RegisterGRPC(server)
	reflection.Register(server)
	listener, err := net.Listen("tcp", a.grpcHost)
	if err != nil {
		return nil, fmt.Errorf("failed to create grpc listener: %w", err)
	}

	go func() {
		err = server.Serve(listener)
		if err != nil {
			a.closeFns.RunAll(fmt.Errorf("failed to start grpc server: %w", err))
		}
	}()

	return server, nil
}
