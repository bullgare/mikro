package mikro

import (
	"fmt"
	"log"

	"github.com/pkg/errors"
	"github.com/utrack/clay/v2/transport"

	"gitlab.com/bullgare/mikro/pkg/closer"
)

type App struct {
	httpHost  string
	grpcHost  string
	adminHost string

	desc     transport.ServiceDesc
	closeFns *closer.ServerCloseFns
}

func NewApp(args AppArgs, descs ...transport.ServiceDesc) *App {
	desc := transport.NewCompoundServiceDesc(descs...)

	return &App{
		httpHost:  args.HTTPHost,
		grpcHost:  args.GRPCHost,
		adminHost: args.AdminHost,
		desc:      desc,
		closeFns:  &closer.ServerCloseFns{},
	}
}

// TODO remove logging via log package
func (a *App) Run() (err error) {
	defer func() {
		if err != nil {
			a.closeFns.RunAll(err)
		}
	}()

	_, err = a.startAdminHTTPServer()
	if err != nil {
		return fmt.Errorf("could not start admin server: %w", err)
	}
	log.Printf("starting admin server on address %s\n", a.adminHost)

	_, err = a.startPublicHTTPServer()
	if err != nil {
		return fmt.Errorf("could not start public http server: %w", err)
	}
	log.Printf("starting http server on address %s\n", a.httpHost)

	_, err = a.startGRPCServer()
	if err != nil {
		return fmt.Errorf("could not start grpc server: %w", err)
	}
	log.Printf("starting grpc server on address %s\n", a.grpcHost)

	signals := closer.CatchSignals()
	select {
	case <-signals:
		a.closeFns.RunAll(errors.New("got termination from command line"))
	}

	return nil
}

func (a *App) GetParams() map[string]string {
	return map[string]string{
		"Name":      Name,
		"Version":   Version,
		"GoVersion": GoVersion,
		"BuildDate": BuildDate,
		"GitLog":    GitLog,
		"GitHash":   GitHash,
		"GitBranch": GitBranch,
	}
}

type AppArgs struct {
	HTTPHost  string
	GRPCHost  string
	AdminHost string
}
