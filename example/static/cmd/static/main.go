package main

import (
	"bitbucket.org/bullgare/go_service_generator/app/vacancies"

	"gitlab.com/bullgare/mikro/pkg/mikro"
)

func main() {
	args := mikro.AppArgs{
		GRPCHost:  "localhost:7000",
		HTTPHost:  "localhost:7001",
		AdminHost: "localhost:7002",
	}
	app := mikro.NewApp(
		args,
		vacancies.NewVacancies().GetDescription(),
	)
	app.Run()
}
