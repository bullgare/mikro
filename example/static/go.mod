module gitlab.com/bullgare/mikro/example/simple

go 1.13

require (
	bitbucket.org/bullgare/go_service_generator v0.0.0+incompatible
	gitlab.com/bullgare/mikro v0.0.0+incompatible
)

replace (
	bitbucket.org/bullgare/go_service_generator => ../../../../../bitbucket.org/bullgare/go_service_generator
	gitlab.com/bullgare/mikro => ../../
)
