package cmd

import (
	"fmt"
	"regexp"

	"github.com/spf13/cobra"

	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/check"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/helpers"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/mainfile"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/makefile"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/out"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/project"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/proto"
)

var initCmd = &cobra.Command{
	Use:     "init",
	Aliases: []string{"initialize", "initialise", "create"},
	Short:   "Initialize an app",
	Long: `This command will create a new application, with the appropriate structure for a golang service based on mikro.
All flags are required.`,
	Run: initCmdRunner,
}

func init() {
	RootCmd.AddCommand(initCmd)

	initCmd.Flags().String("module", "", "module name for go.mod (like gitlab.com/bullgare/mikro/example/simple)")
	initCmd.Flags().String("app_name", "", "application (service) name (optional); (e.g. simple)")
	initCmd.Flags().String("proto", "", "path to proto file(s) for generating your service. Separate paths with ':'")
}

func initCmdRunner(cmd *cobra.Command, _ []string) {
	var proj *project.Project
	module := cmd.Flag("module").Value.String()
	appName := cmd.Flag("app_name").Value.String()
	protoPath := cmd.Flag("proto").Value.String()

	proj = project.New(module, appName)

	if err := validateAppName(proj.Name()); err != nil {
		out.Fatalf("bad project name (last part of module): %s", err)
	}
	if err := validateAppName(proj.AppName()); err != nil {
		out.Fatalf("bad app_name: %s", err)
	}

	if errs := check.Versions(verbose); len(errs) > 0 {
		for _, err := range errs {
			out.Error(err.Error())
		}
		out.Fatal("system requirements above were not satisfied")
	}

	proto.Process(proj, protoPath)
	makefile.Create(proj)

	out.Info("running `make generate`")
	generateErr := helpers.ExecMakefile("generate", proj.AbsPath())

	mainfile.Create(proj)

	out.Info("running `make deps`")
	depsErr := helpers.ExecMakefile("deps", proj.AbsPath())

	if generateErr != nil || depsErr != nil {
		out.Warn("Initialization passed with errors")
		out.Warnf(`Your new service is placed at %#q.
			But some errors occurred.
			Fix error and run "make generate && make deps"
			Then you can run application "go run cmd/%s/main.go"\n\n`, proj.AbsPath(), proj.Name())
	} else {
		out.Info("Init succeeded")
		out.Infof(`your mikro service was inited successfully. It is placed at %#q.
			You can run application with "go run cmd/%s/main.go"\n\n`, proj.AbsPath(), proj.Name())
	}
}

var appNameRegexp = regexp.MustCompile(`^[a-zA-Z][a-zA-Z0-9-]*$`)

func validateAppName(val string) error {
	if !appNameRegexp.MatchString(val) {
		return fmt.Errorf("%q contains forbidden symbols, only alphanumeric and '-' are allowed", val)
	}
	return nil
}
