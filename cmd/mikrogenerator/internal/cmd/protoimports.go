package cmd

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/check"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/helpers"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/out"
)

func init() {
	protoImportsCmd.Flags().String("out_dir", "third_party/proto", "directory to store resolved imports")
	protoImportsCmd.Flags().String("proto", "", "path to proto file")
	RootCmd.AddCommand(protoImportsCmd)
}

var protoImportsCmd = &cobra.Command{
	Use:   `protoimports`,
	Short: `Recursively stores imports from proto into --out_dir directory`,

	Run: func(cmd *cobra.Command, args []string) {
		if errs := check.Versions(false); len(errs) > 0 {
			for _, err := range errs {
				out.Error(err.Error())
			}
			out.Fatal("check the requirements")
		}

		outDir := cmd.Flag("out_dir").Value.String()
		pathToProto := cmd.Flag("proto").Value.String()
		pathToProto, _ = filepath.Abs(pathToProto)
		if filepath.Ext(pathToProto) != ".proto" {
			out.Fatal("file in --proto should point at proto file")
		}
		if !helpers.IsExist(pathToProto) {
			out.Fatalf("failed to find file %s", pathToProto)
		}
		deps := map[string]dep{}
		if err := setDeps(deps); err != nil {
			out.Fatalf("setDeps failed: %v", err)
		}

		if err := copyImportsFromSource(pathToProto, outDir, deps); err != nil {
			out.Fatalf("copyImportsFromSource failed: %v", err)
		}

		if output, err := exec.Command("go", "mod", "tidy").CombinedOutput(); err != nil {
			out.Fatalf("failed to run go mod tidy: (%v):\n%s", err, output)
		}
	},
}

func copyImportsFromSource(pathToProto, outDir string, deps map[string]dep) error {
	var (
		re           = regexp.MustCompile(`^import "(.*?)";$`)
		knownImports = map[string]bool{}
	)

	protoFile, err := os.Open(pathToProto)
	if err != nil {
		return err
	}
	defer protoFile.Close()

	scanner := bufio.NewScanner(protoFile)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		if m := re.FindStringSubmatch(scanner.Text()); len(m) > 1 {
			protoImport := m[1]
			if verbose {
				out.Infof("found proto import %s in file %s", protoImport, pathToProto)
			}
			err = processOneImport(protoImport, outDir, deps, knownImports)
			if err != nil {
				return fmt.Errorf("failed to process import %s from protofile %s", protoImport, pathToProto)
			}
		}
	}
	if err := scanner.Err(); err != nil {
		return fmt.Errorf("failed to scan %s: %w", pathToProto, err)
	}
	return nil
}

func processOneImport(proto string, outDir string, deps map[string]dep, knownImports map[string]bool) error {
	filename := filepath.Base(proto)
	if knownImports[proto] {
		if verbose {
			out.Infof("already processed, skipping")
		}
		return nil
	}
	knownImports[proto] = true

	pathSeparator := string(filepath.Separator)

	protoImport := strings.TrimSuffix(proto, pathSeparator+filename)
	if verbose {
		out.Infof("found proto import package %s", protoImport)
	}
	pkgSlugs := strings.Split(protoImport, pathSeparator)

	var repoFound bool
	var dependency dep
	var repoPkg string

	for repoSlugsCount := len(pkgSlugs); !repoFound && repoSlugsCount >= 1; repoSlugsCount-- {
		repo := strings.Join(pkgSlugs[:repoSlugsCount], pathSeparator)
		// checking /v2, /v3, etc
		if len(pkgSlugs) > repoSlugsCount && strings.HasPrefix(pkgSlugs[repoSlugsCount], "v") {
			if _, err := strconv.Atoi(strings.TrimPrefix(pkgSlugs[repoSlugsCount], "v")); err == nil {
				repo += pathSeparator + pkgSlugs[repoSlugsCount]
			}
		}
		if verbose {
			out.Infof("trying repo %s", repo)
		}

		repoPkg = strings.TrimPrefix(strings.TrimPrefix(protoImport, repo), pathSeparator)
		if verbose {
			out.Infof("trying repo package %s", repoPkg)
		}

		if r1, r2, replaced := replaceRepo(repo); replaced {
			if verbose {
				out.Infof("repo was replaced: %s -> %s", repo, r1)
				out.Infof("repo package was replaced: %s -> %s", repoPkg, r2)
			}
			repo = r1
			repoPkg = r2
		}

		dependency, repoFound = deps[repo]
		if !repoFound || dependency.Dir == "" || !helpers.IsExist(filepath.Join(dependency.Dir, repoPkg, filename)) {
			version := "latest"
			if dependency.Version != "" {
				version = dependency.Version
			}
			if err := goGet(repo + "@" + version); err != nil {
				out.Warnf("could not go get repo %s: %v", repo, err)
				continue
			}
			if err := setDeps(deps); err != nil {
				out.Warnf("could not find repo %s: %v", repo, err)
				continue
			}

			var ok bool
			dependency, ok = deps[repo]
			if ok && dependency.Dir != "" && helpers.IsExist(filepath.Join(dependency.Dir, repoPkg, filename)) {
				repoFound = true
			} else if verbose {
				out.Infof("found repo %s, but source file %s not found", dependency.Path, filepath.Join(repoPkg, filename))
			}
		}
	}
	if !repoFound {
		return fmt.Errorf("could not find repo for import %s", proto)
	}

	if verbose {
		out.Infof("found %s, copying %s -> %s", proto, filepath.Join(dependency.Dir, repoPkg, filename), filepath.Join(outDir, protoImport, filename))
	}
	if err := os.MkdirAll(filepath.Join(outDir, protoImport), os.ModePerm); err != nil {
		return fmt.Errorf("failed to create dir %s: %w", filepath.Join(outDir, protoImport), err)
	}
	f, err := os.Open(filepath.Join(dependency.Dir, repoPkg, filename))
	if err != nil {
		return fmt.Errorf("failed to open file %s: %w", filepath.Join(dependency.Dir, repoPkg, filename), err)
	}
	defer f.Close()
	err = helpers.WriteToFile(filepath.Join(outDir, protoImport, filename), f)
	if err != nil {
		return fmt.Errorf("failed to write file %s: %w", filepath.Join(outDir, protoImport, filename), err)
	}

	return copyImportsFromSource(filepath.Join(outDir, protoImport, filename), outDir, deps)
}

func replaceRepo(repo string) (replacedRepo, pkg string, replaced bool) {
	switch repo {
	case "google/api":
		return "github.com/googleapis/googleapis", "google/api", true
	case "google/protobuf":
		return "github.com/google/protobuf", "src/google/protobuf", true
	case "google/rpc":
		return "github.com/googleapis/googleapis", "google/rpc", true
	case "google/type":
		return "github.com/googleapis/googleapis", "google/type", true
	case "protoc-gen-swagger/options":
		return "github.com/grpc-ecosystem/grpc-gateway", "protoc-gen-swagger/options", true
	default:
		return "", "", false
	}
}

type dep struct {
	Path    string `json:"Path"`
	Version string `json:"Version"`
	Dir     string `json:"Dir"`
}

func setDeps(deps map[string]dep) error {
	cmd := exec.Command("go", "list", "-m", "-json", "all")
	stdOut, err := cmd.StdoutPipe()
	if err != nil {
		return fmt.Errorf("failed to pipe go list: %w", err)
	}
	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("failed to start go list: %w", err)
	}
	scanner := bufio.NewScanner(stdOut)
	scanner.Split(jsonSplitter)
	for scanner.Scan() {
		d := dep{}
		if err := json.Unmarshal(scanner.Bytes(), &d); err != nil {
			return fmt.Errorf("failed to unmarshal package info: %w", err)
		}
		deps[d.Path] = d
	}
	if err := scanner.Err(); err != nil {
		return fmt.Errorf("failed to read result from go list: %w", err)
	}
	return nil
}

func goGet(repo string) error {
	if verbose {
		out.Infof("go get %s", repo)
	}
	cmd := exec.Command("go", "get", repo)
	response, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("failed to go get %s (%w):\n%s", repo, err, response)
	}
	return nil
}

func jsonSplitter(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if i := bytes.Index(data, []byte{'}', '\n'}); i >= 0 {
		return i + 2, data[0 : i+1], nil
	}
	if !atEOF {
		return 0, nil, nil
	}
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	return len(data), data, nil
}
