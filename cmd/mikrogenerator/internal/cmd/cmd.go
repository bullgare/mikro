package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/out"
)

var (
	RootCmd = &cobra.Command{
		Use:   "mikrogenerator",
		Short: "A generator for applications powered by mikro",
		Long: `mikrogenerator is a CLI tool for generating GO apps based on gitlab.com/bullgare/mikro.
It generates all files for the app with the correct structure according to proto file(s).`,
	}
)

var verbose = false

func init() {
	RootCmd.PersistentFlags().BoolP("verbose", "v", false, "more messages")
	cobra.OnInitialize(initGlobalFlags)
}

func initGlobalFlags() {
	var err error
	verbose, err = RootCmd.Flags().GetBool("verbose")
	if err != nil {
		out.Warnf("can't get `verbose` flag: %s", err)
	}

	if verbose {
		out.Info("verbose mode on")
	}
}
