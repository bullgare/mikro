package templates

import (
	"strconv"
	"strings"
)

// MainData is a datastruct for main.go
type MainData struct {
	Data

	PortHTTP  int
	PortGRPC  int
	PortAdmin int

	Imports map[string]Import
}

// Pkg returns pkg prefix with dot
func (d MainData) Pkg(pkg string) string {
	i := d.Imports[pkg]
	if i.Alias != "" {
		return i.Alias + `.`
	}
	return ""
}

// AddImport adds pkg to Imports map, and guarantees a unique alias
// if alias[0] passed it will be used as alias for this pkg, alias[1..N] are ignored
func (d *MainData) AddImport(pkg string, alias ...string) *MainData {
	if d.Imports == nil {
		d.Imports = make(map[string]Import)
	}
	if len(alias) != 0 {
		d.Imports[pkg] = Import{Pkg: pkg, Alias: alias[0]}
		return d
	}

	path := strings.Split(pkg, "/")
	if len(path) == 0 {
		return d
	}
	lastPart := path[len(path)-1]
	lastPart = strings.Replace(lastPart, "-", "_", -1)
	a := func(i int) string {
		if i == 1 {
			return lastPart
		}
		return lastPart + strconv.Itoa(i)
	}
	n := 1
	found := false
	for !found {
		for _, i := range d.Imports {
			if i.Alias == a(n) {
				n++
				break
			}
		}
		found = true
	}
	d.Imports[pkg] = Import{Pkg: pkg, Alias: a(n)}
	return d
}

type Import struct {
	Alias string
	Pkg   string
}

const MainTemplate = `package main

import (
	{{ range $import := .Imports -}}
		{{ $import.Alias }} "{{ $import.Pkg }}"
	{{ end }}
)

func main() {
	args := {{ pkg "gitlab.com/bullgare/mikro/pkg/mikro" }}AppArgs{
		HTTPHost:  "localhost:{{ .PortHTTP }}",
		GRPCHost:  "localhost:{{ .PortGRPC }}",
		AdminHost: "localhost:{{ .PortAdmin }}",
	}
	app := {{ pkg "gitlab.com/bullgare/mikro/pkg/mikro" }}NewApp(
		args,
		{{range $proto := .Protos -}}
        {{ pkg $proto.Service.Package }}New{{ index $proto.Service.Name }}().GetDescription(),
		{{- end}}
	)

	err := app.Run()
	if err != nil {
		log.Fatal(err)
	}
}
`
