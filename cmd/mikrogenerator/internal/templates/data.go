package templates

// Data is a datastruct containing common stuff for the templates
type Data struct {
	Protos []Proto
	// AppNameUnderscoredUpper is an UPPERCASE underscored project proto
	AppNameUnderscoredUpper string
	// AppName is application name
	AppName string
	// CmdName is a project name
	CmdName string
	// Go Module
	Module string
}

type Proto struct {
	// RelativeDir - path to your proto file rel project root
	RelativeDir string
	// Name is a Name of proto file without ext
	Name string
	// Service is a service from proto file
	Service *Service
}

type Service struct {
	Name                string
	UpperSnakeCasedName string
	KebabCasedName      string
	Package             string
}
