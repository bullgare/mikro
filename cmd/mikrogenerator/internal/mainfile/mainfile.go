package mainfile

import (
	"path/filepath"

	"golang.org/x/tools/imports"

	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/helpers"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/out"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/project"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/templates"
)

const (
	portHttp  = 3000
	portGRPC  = 3001
	portAdmin = 3002
)

func Create(proj *project.Project) {
	absPath := filepath.Join(proj.AbsPath(), "cmd", proj.Name(), "main.go")
	if !helpers.ConfirmOverwrite(absPath) {
		return
	}

	data := templates.MainData{
		Data: templates.Data{
			Protos: proj.Protos(),
		},
		PortHTTP:  portHttp,
		PortGRPC:  portGRPC,
		PortAdmin: portAdmin,
	}
	(&data).
		AddImport("context").
		AddImport("gitlab.com/bullgare/mikro/pkg/mikro")

	for _, p := range proj.Protos() {
		(&data).AddImport(p.Service.Package)
	}

	mainScript, err := templates.Execute(templates.MainTemplate, data)
	if err != nil {
		out.Fatal(err)
	}

	formattedBytes, err := imports.Process("", []byte(mainScript), nil)
	if err != nil {
		out.Fatalf("failed to apply goimports for main.go: %s\ncontents:\n%s\n", err, mainScript)
	}
	mainScript = string(formattedBytes)

	err = helpers.WriteStringToFile(absPath, mainScript)
	if err != nil {
		out.Fatal(err)
	}
}
