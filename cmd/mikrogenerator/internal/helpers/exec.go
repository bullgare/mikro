package helpers

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/fatih/color"
)

func ExecMakefile(target, path string) error {
	return Exec(true, true, "make", "-C", path, target)
}

func Exec(do bool, print bool, name string, args ...string) error {
	if print {
		printCommand(name, args...)
	}
	if !do {
		return nil
	}
	cmd := exec.Command(name, args...)
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Printf("failed to execute command: %s\n", color.RedString(err.Error()))
		return err
	}
	return nil
}

func printCommand(name string, aa ...string) {
	format := "%s"
	var args []interface{}
	args = append(args, name)
	for _, a := range aa {
		// HEREDOC syntax should not be escaped
		if strings.Contains(a, "cat << EOF") {
			format += " %s"
		} else if strings.Contains(a, " ") {
			format += " %q"
		} else {
			format += " %s"
		}
		args = append(args, a)
	}
	fmt.Println(color.YellowString(format, args...))
}
