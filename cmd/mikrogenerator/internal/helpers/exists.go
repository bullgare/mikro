package helpers

import (
	"errors"
	"os"
)

func Exists(path string) error {
	if path == "" {
		return errors.New("empty path")
	}
	_, err := os.Stat(path)
	if err == nil {
		return nil
	}
	if !os.IsNotExist(err) {
		return err
	}
	return nil
}

func IsExist(path string) bool {
	return Exists(path) == nil
}
