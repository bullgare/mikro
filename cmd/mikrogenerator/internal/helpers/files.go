package helpers

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/manifoldco/promptui"

	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/out"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/templates"
)

func ExecAndWriteTplToFile(path string, tpl string, data interface{}) error {
	text, err := templates.Execute(tpl, data)
	if err != nil {
		return err
	}

	return WriteStringToFile(path, text)
}

func WriteStringToFile(path string, s string) error {
	return WriteToFile(path, strings.NewReader(s))
}

// WriteToFile writes r to the file with path
func WriteToFile(path string, r io.Reader) error {
	dir := filepath.Dir(path)
	if dir != "" {
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			return err
		}
	}

	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, r)
	return err
}

func ConfirmOverwrite(path string) bool {
	if _, err := os.Stat(path); err != nil {
		return true
	}

	var (
		keep  = fmt.Sprintf("Keep current version of `%s`", path)
		write = fmt.Sprintf("Write mikro default version of `%s`", filepath.Base(path))
	)

	prompt := promptui.Select{
		Label: fmt.Sprintf("`%s` already exists, do you want to keep this file", filepath.Base(path)),
		Items: []string{keep, write},
	}

	_, result, err := prompt.Run()

	if err == promptui.ErrInterrupt {
		os.Exit(130)
	}

	if err != nil {
		out.Errorf("Prompt failed %v: %s", err, keep)
		return false
	}

	if result == write {
		return true
	}
	return false
}
