package out

import (
	"fmt"
	"os"

	"github.com/fatih/color"
)

func Fatal(msg ...interface{}) {
	Fatalf(fmt.Sprint(msg...))
}

func Error(msg ...interface{}) {
	Errorf(fmt.Sprint(msg...))
}

func Warn(msg ...interface{}) {
	Warnf(fmt.Sprint(msg...))
}

func Info(msg ...interface{}) {
	Infof(fmt.Sprint(msg...))
}

func Print(msg ...interface{}) {
	Printf(fmt.Sprint(msg...))
}

func Fatalf(f string, v ...interface{}) {
	fmt.Printf(color.RedString("Fatal error: ")+f+"\n", v...)
	os.Exit(1)
}

func Errorf(f string, v ...interface{}) {
	fmt.Printf(color.RedString("Error: ")+f+"\n", v...)
}

func Warnf(f string, v ...interface{}) {
	fmt.Printf(color.YellowString("Warning: ")+f+"\n", v...)
}

func Infof(f string, v ...interface{}) {
	fmt.Printf(color.GreenString("Info: ")+f+"\n", v...)
}

func Printf(f string, v ...interface{}) {
	fmt.Printf("Info: "+f+"\n", v...)
}
