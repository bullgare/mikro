package project

import (
	"os"
	"strings"

	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/out"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/templates"
)

// Project contains name and paths to projects.
type Project struct {
	absPath     string
	module      string
	name        string
	appName     string
	protos      []templates.Proto
	jiraProject string
}

// New returns Project with specified module.
func New(module string, appName string) *Project {
	if module == "" {
		out.Fatal("can't create project with empty module")
	}

	p := new(Project)
	p.module = module
	parts := strings.Split(p.module, "/")
	p.name = parts[len(parts)-1]

	p.appName = appName
	if appName == "" {
		p.appName = p.name
	}

	wd, err := os.Getwd()
	if err != nil {
		out.Fatal(err)
	}
	p.absPath = wd

	return p
}

// Module returns the go module name of the project, e.g. "github.com/spf13/cobra"
func (p *Project) Module() string {
	return p.module
}

// Name returns the last part of module name, e.g. "github.com/spf13/cobra" -> returns "cobra"
func (p *Project) Name() string {
	return p.name
}

// AppName returns the application name, which is just Name if not specified explicitly
func (p *Project) AppName() string {
	return p.appName
}

// AbsPath returns absolute path of project.
func (p *Project) AbsPath() string {
	return p.absPath
}

// SetProtos i a getter
func (p *Project) SetProtos(protos []templates.Proto) {
	p.protos = protos
}

// Protos i a getter
func (p *Project) Protos() []templates.Proto {
	return p.protos
}

// NameUnderscoredUpper returns module name in UPPER case with "-" -> "_" replaced
func (p *Project) NameUnderscoredUpper() string {
	return strings.ToUpper(strings.Replace(p.name, "-", "_", -1))
}
