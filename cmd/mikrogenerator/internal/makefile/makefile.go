package makefile

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"github.com/Masterminds/semver"
	"github.com/fatih/color"

	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/helpers"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/out"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/project"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/templates"
)

var projectStructureImplRegExp = regexp.MustCompile(`^PROJECT_STRUCTURE_IMPLEMENTATIONS\s*=\s*(.*)$`)

func GetProjectStructureImpl(proj *project.Project) string {
	mk := filepath.Join(proj.AbsPath(), "Makefile")
	var projectStructureImpl string
	if _, err := os.Stat(mk); err == nil {
		makefile, err := os.Open(mk)
		if err != nil {
			out.Fatal(err)
		}
		defer makefile.Close()

		scanner := bufio.NewScanner(makefile)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			for _, p := range proj.Protos() {
				svc := p.Service
				if m := regexp.MustCompile(
					fmt.Sprintf(`^PROJECT_STRUCTURE_%s_IMPLEMENTATION\s*=\s*(.*)$`, regexp.QuoteMeta(svc.UpperSnakeCasedName)),
				).FindStringSubmatch(scanner.Text()); len(m) > 1 {
					svc.Package = filepath.Join(proj.Module(), m[1])
					break
				}
			}
			if projectStructureImpl == "" {
				if m := projectStructureImplRegExp.FindStringSubmatch(scanner.Text()); len(m) > 1 {
					projectStructureImpl = m[1]
				}
			}
		}
		if err := scanner.Err(); err != nil {
			fmt.Println(color.RedString("failed to read Makefile:"), err.Error())
		}
	}
	if projectStructureImpl == "" {
		projectStructureImpl = filepath.Join("internal", "app")
	}

	return projectStructureImpl
}

func Create(proj *project.Project) {
	version := internal.Version
	data := templates.MikroMakeData{
		Data: templates.Data{
			Protos:  proj.Protos(),
			AppName: proj.AppName(),
			CmdName: proj.Name(),
			Module:  proj.Module(),
		},
		MikroGenVersion:       version,
		MikroBootstrapVersion: internal.MikroMinVersion,
	}

	_, err := semver.NewVersion(version)
	if err != nil {
		out.Warnf("mikrogenerator version %q is not using SemVer", version)
	}
	data.MikroVersion = version

	data.MikroLibRef = version
	data.MikroVersion = version

	mikroMkPath := filepath.Join(proj.AbsPath(), "mikro.mk")
	err = helpers.ExecAndWriteTplToFile(mikroMkPath, templates.MikroMake, data)
	if err != nil {
		out.Fatal(err)
	}

	makefilePath := filepath.Join(proj.AbsPath(), "Makefile")
	if !helpers.ConfirmOverwrite(makefilePath) {
		return
	}

	err = helpers.ExecAndWriteTplToFile(makefilePath, templates.Makefile, data)
	if err != nil {
		out.Fatal(err)
	}
}
