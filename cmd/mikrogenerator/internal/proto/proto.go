package proto

import (
	"bufio"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"unicode"

	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/helpers"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/makefile"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/out"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/project"
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/templates"
)

var serviceRegexp = regexp.MustCompile("^service (.*?) {")

func Process(proj *project.Project, flagWithProto string) {
	fillProtos(proj, flagWithProto)

	projectStructureImpl := makefile.GetProjectStructureImpl(proj)

	for _, p := range proj.Protos() {
		svc := p.Service
		svc.Package = filepath.Join(proj.Module(), projectStructureImpl, svc.KebabCasedName)
	}
}

func fillProtos(proj *project.Project, flagWithProto string) {
	var protos []templates.Proto
	for _, proto := range strings.Split(flagWithProto, string(os.PathListSeparator)) {
		protoAbsPath, _ := filepath.Abs(proto)
		protoExt := filepath.Ext(proto)
		if protoExt != ".proto" {
			out.Fatalf("please specify path to your proto file %s, use --proto flag", protoAbsPath)
		}
		if err := helpers.Exists(protoAbsPath); err != nil {
			out.Fatalf("proto file '%s' does not exist: %v", protoAbsPath, err)
		}
		protoName := strings.TrimSuffix(filepath.Base(proto), protoExt)
		if protoName == "" {
			out.Fatalf("proto file '%s': file name should not be empty", protoAbsPath)
		}
		var protoRelDir string
		if !strings.HasPrefix(protoAbsPath, proj.AbsPath()+string(filepath.Separator)) {
			protoRelDir = "api"
		} else {
			protoRelDir = strings.TrimPrefix(strings.TrimPrefix(filepath.Dir(protoAbsPath), proj.AbsPath()), string(filepath.Separator))
		}
		if protoRelDir != "" {
			protoRelDir += string(filepath.Separator)
		}
		protoProjectPath := filepath.Join(proj.AbsPath(), protoRelDir, protoName+protoExt)
		func() {
			protoFile, err := os.Open(protoAbsPath)
			if err != nil {
				out.Fatal(err)
			}
			defer protoFile.Close()

			// copy proto file into the root of the project if it is not there
			if protoAbsPath != protoProjectPath {
				if err := helpers.WriteToFile(protoProjectPath, protoFile); err != nil {
					out.Fatal(err)
				}
				protoFile.Seek(0, 0)
			}

			p := templates.Proto{Name: protoName, RelativeDir: protoRelDir}

			scanner := bufio.NewScanner(protoFile)
			scanner.Split(bufio.ScanLines)
			for scanner.Scan() {
				if m := serviceRegexp.FindStringSubmatch(scanner.Text()); len(m) > 1 {
					s := templates.Service{Name: m[1]}
					s.KebabCasedName = kebabCase(s.Name)
					s.UpperSnakeCasedName = strings.ToUpper(snakeCase(s.Name))
					if p.Service == nil {
						p.Service = &s
					} else {
						out.Fatalf("More than one Service found in proto: %#q. Split this proto file, one proto for each service!", protoAbsPath)
					}
				}
			}
			if p.Service == nil {
				out.Fatalf("Service not found in proto: %#q", protoAbsPath)
			}
			protos = append(protos, p)
		}()
	}

	if len(protos) == 0 {
		out.Fatalf("no .proto files found in %s (--proto flag)", flagWithProto)
	}

	proj.SetProtos(protos)
}

func kebabCase(s string) string {
	return strings.Trim(strings.Replace(snakeCase(s), "_", "-", -1), "-")
}

// SnakeCase converts the given string to snake case following the Golang format:
// acronyms are converted to lower-case and preceded by an underscore.
// copied from https://github.com/azer/snakecase/blob/master/snakecase.go
func snakeCase(s string) string {
	in := []rune(s)
	isLower := func(idx int) bool {
		return idx >= 0 && idx < len(in) && unicode.IsLower(in[idx])
	}

	output := make([]rune, 0, len(in)+len(in)/2)
	for i, r := range in {
		if unicode.IsUpper(r) {
			r = unicode.ToLower(r)
			if i > 0 && in[i-1] != '_' && (isLower(i-1) || isLower(i+1)) {
				output = append(output, '_')
			}
		}
		output = append(output, r)
	}

	return string(output)
}
