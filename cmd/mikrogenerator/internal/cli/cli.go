package cli

import (
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/cmd"
)

func Start() {
	cmd.RootCmd.Execute()
}
