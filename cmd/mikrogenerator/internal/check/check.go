package check

import (
	"fmt"
	"os/exec"
	"regexp"

	"github.com/Masterminds/semver"

	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/out"
)

var checkers = []struct {
	name       string
	minVersion string
	checker    func(string) error
}{
	{
		name:       "go",
		minVersion: "1.13.0",
		checker:    goVersion,
	},
	{
		name:       "protoc",
		minVersion: "3.3.0",
		checker:    protocVersion,
	},
	{
		name:       "git",
		minVersion: "2.3.4",
		checker:    gitVersion,
	},
}

func Versions(verbose bool) []error {
	var (
		err  error
		errs []error
	)

	for _, c := range checkers {
		if verbose {
			out.Infof("checking %s version", c.name)
		}

		err = c.checker(c.minVersion)
		if err != nil {
			errs = append(errs, fmt.Errorf("%s: %w", c.name, err))
		}
	}

	return errs
}

func goVersion(version string) error {
	cmd := exec.Command("go", "version")
	return checkSemVerFrom(cmd, version)
}

func protocVersion(version string) error {
	cmd := exec.Command("protoc", "--version")
	return checkSemVerFrom(cmd, version)
}

func gitVersion(version string) error {
	cmd := exec.Command("git", "version")
	return checkSemVerFrom(cmd, version)
}

// checkSemVerFrom check version requirements based on semver
// cmd - command for printing version, minReqVersion - minimal require version, utilName - utility name for error message
func checkSemVerFrom(cmd *exec.Cmd, minReqVersion string) error {
	output, err := cmd.Output()
	if err != nil {
		return err
	}

	version, err := extractSemver(string(output))
	if err != nil {
		return err
	}

	v, err := semver.NewVersion(version)
	if err != nil {
		fmt.Print("semver err", err)
	}

	c, _ := semver.NewConstraint(">= " + minReqVersion)
	if !c.Check(v) {
		return fmt.Errorf("version should be >= " + minReqVersion + " current version is " + v.String())
	}

	return nil
}

var semverRegexp = regexp.MustCompile(`([0-9]+)\.([0-9]+)(\.([0-9]+))?(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?`)

// extractSemver check if string consist semver and return it
func extractSemver(s string) (string, error) {
	matches := semverRegexp.FindStringSubmatch(s)
	if len(matches) > 0 {
		return matches[0], nil
	}

	return "", fmt.Errorf("`%s` does not contain semantic version", s)
}
