module gitlab.com/bullgare/mikro/cmd/mikrogenerator

go 1.13

require (
	github.com/Masterminds/semver v1.5.0
	github.com/fatih/color v1.9.0
	github.com/manifoldco/promptui v0.7.0
	github.com/spf13/cobra v0.0.5
	golang.org/x/tools v0.0.0-20200131211209-ecb101ed6550
	google.golang.org/appengine v1.6.6
)
