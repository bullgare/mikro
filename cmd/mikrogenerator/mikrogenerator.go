package main

import (
	"gitlab.com/bullgare/mikro/cmd/mikrogenerator/internal/cli"
)

func main() {
	cli.Start()
}
