DIR_SWAGGER_UI:=$(CURDIR)/swagger

.PHONY: install-swagger-ui
install-swagger-ui:
	$(info #Compiling swagger-ui...)
	go get -u github.com/bullgare/gostatic2lib

	rm -rf /tmp/swagger-ui
	git clone https://github.com/swagger-api/swagger-ui.git /tmp/swagger-ui

	cd /tmp/swagger-ui; \
		mkdir ./html; \
		cat ./dist/index.html | perl -pe 's/https?:\/\/petstore.swagger.io\/v2\///g' > ./html/index.html; \
		cp ./dist/*.js ./html; \
		cp ./dist/*.css ./html; \
		cp ./dist/*.png ./html

	mkdir -p $(DIR_SWAGGER_UI)
	gostatic2lib -out $(DIR_SWAGGER_UI)/static.go -package swagger -path /tmp/swagger-ui/html

	rm -rf /tmp/swagger-ui
